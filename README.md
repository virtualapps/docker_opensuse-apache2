use this image to host your static web pages using apache2

start this container by command:
docker run -d -p <host port to bind>:80 virtualapps/opensuse-apache2

example:
docker run -d  -p 4243:80 virtualapps/opensuse-apache2

you can access your web service by pointing your browser to url: http://<hostname>:<host port to bind>

In the above example access url is http://hostname:4243

copy your file to /srv/www/htdocs/ directory.